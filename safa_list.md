# Типовой список замечаний при проверках ВС

Ниже приведён типовой список отклонений выявляемых в рамках проверок SAFA/SANA, а также во время внутренних проверок ВС.

С типовым списком групп угроз можно ознакомиться в разделе [](https://gitlab.com/avex-bureau/docs), файл [Типовой список негативных факторов, классов и групп угроз](hazards.md).

Процесс формирования списка областей недостатка описан в статье [Классификация сообщений по безопасности полётов](https://avex.pro/2020/07/12/safety-reports-classification.html).

Указанные ниже типовые замечания рекомендуется внести в перечень классификаторов ASL, указав тип классификатора - "Предпосылка"

|Название|Перевод|Области недостатка|
|---|---|---|
| A01: Состояние пилотской кабины | A01: Flight Deck General Condition | ['MNT'] |
| A02: Аварийные выходы | A02: Emergency Exits | ['MNT'] |
| A03: Оборудование пилотской кабины | A03: Flight Deck Equipment | ['MNT'] |
| A04: Руководства | A04: Manuals | ['FLT', 'MNT', 'CAB'] |
| A05: Чек-листы пилотской кабины | A05: Checklists | ['FLT'] |
| A06: Радионавигационные карты | A06: Radio Navigation Charts | ['MNT'] |
| A07: Minimum Equipment List | A07: Minimum Equipment List | ['MNT'] |
| A08: Cертификат Регистрации ВС | A08: Certificate of Registration | ['MNT'] |
| A09: Сертификат по шумам | A09: Noise Certificate | ['MNT'] |
| A10: Сертификат эксплуатанта или эквивалент | A10: AOC or equivalent | ['MNT'] |
| A11: Лицензии на радиостанции | A11: Radio Licence | ['MNT'] |
| A12: Cертификат летной годности | A12: Certificate of Airworhiness | ['MNT'] |
| A13 Подготовка к вылету | A13 Flight Preparation | ['FLT', 'MNT', 'CAB', 'DSP', 'FNS'] |
| A14: Неправильный расчет веса и центровки | A14: Improper weight and balance calculations | ['FLT'] |
| A15: Переносные пожарные баллоны в пилотской кабине | A15: Cockpit Hand fire Extinguishers | ['MNT'] |
| A16: Спасательные жилеты членов экипажа | A16: Flight crew Life Jackets | ['MNT'] |
| A17: Плечевые и безопасные ремни | A17: Harness | ['MNT'] |
| A18: Кислородное оборудование в кабине экипажа | A18: Oxygen Equipment | ['MNT'] |
| A19: Освещение пилотской кабины | A19: Flash light | ['MNT'] |
| A20: Пилотские свидетельства | A20: Flight Crew Licence | ['FLT'] |
| A21: полётное задание | A21: Journey Log | ['FLT'] |
| A22: Допуск к эксплуатации | A22: Maintenance Release | ['MNT'] |
| A23: Неодобренный ремонт | A23: Not approved repair | ['MNT'] |
| A24: Предполётный осмотр | A24: Preflight inspection | ['FLT'] |
| B01: Безопасность пассажирской кабины - Общие состояние | B01: Cabin Safety - General condition | ['MNT'] |
| B02: Состояние рабочих мест и мест отдыха бортпроводников | B02: Cabin Attendant stations/rest area | ['MNT'] |
| B03: Аптечка, комплект первой помощи | B03: First Aid Kit/Emergency Medical Kit | ['EQP'] |
| B04: Переносные пожарные баллоны салона | B04: Cabin Hand fire Extinguishers | ['MNT'] |
| B05: Спасательные жилеты, флотационные устройства | B05: Life jackets/Flotation devices | ['MNT'] |
| B06: Привязные ремни и состояние кресел салона | B06: Seat belts and seat condition | ['MNT'] |
| B07: Аварийные выходы салона | B07: Emergency exits | ['MNT'] |
| B08: Аварийный трап, спасательный плот, ELT | B08: Slides/Life Rafts, ELT | ['MNT'] |
| B09. Подача кислорода | B09. Oxygen Supply | ['MNT'] |
| B10: Инструкции по безопасности / АСЖ | B10: Safety instructions | ['MNT', 'SEC'] |
| B11: Кабинный экипаж | B11: Cabin crew members | ['CAB'] |
| B12: Доступ к аварийным выходам | B12: Access to emergency exits | ['MNT', 'CAB'] |
| B13 Безопасность пассажирского багажа | B13 Safety of passenger baggage | ['MNT'] |
| B14: Количество посадочных мест | B14: Seat capacity | ['CAB'] |
| C01: Общее внешнее состояние ВС | C01: General external conditions | ['MNT'] |
| C02: Двери и люки | C02: Doors and Hatches | ['MNT'] |
| C03: Системы управления полётом | C03: Flight Controls | ['MNT'] |
| C04: Колеса/шины/тормоза | C04: Wheels/Tyres/Brackes | ['MNT'] |
| C05: Шасси | C05: Undercarriage | ['MNT'] |
| C06: Нишы шасси | C06: Wheel Well | ['MNT'] |
| C07: Двигатели/Пилоны | C07: Powerplant/pylons | ['MNT'] |
| C08: Повреждения лопаток вентилятора | C08: Fan Blade Damage | ['MNT'] |
| C10: Видимый ремонт | C10: Obvious repairs | ['MNT'] |
| C11: Не выполненные ремонты повреждений | C11: Unrepaired Aircraft Damage | ['MNT'] |
| C12: Течи | C12: Leakage | ['MNT'] |
| D01: Общее состояние грузовых отсеков | D01: General Condition of Cargo Compartment | ['MNT'] |
| D02: Опасные грузы | D02: Dangerous Goods | ['FLT', 'SEC', 'PAS', 'CGO'] |
| D03: Безопасность груза на борту | D03: Safety of Cargo on Board | ['FLT'] |
| E01: Общее | E01: General | ['FLT', 'MNT', 'CAB', 'DSP', 'FNS', 'CLN', 'CGO', 'EQP'] |
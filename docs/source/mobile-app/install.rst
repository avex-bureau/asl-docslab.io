Установка мобильного приложения
-------------------------------

Мобильное приложение ASL доступна для установки по следующей ссылке: https://app.avex.pro.

Установка в Google Chrome
~~~~~~~~~~~~~~~~~~~~~~~~~

Для установки мобильного приложения в Google Chrome, необходимо перейти по ссылке https://app.avex.pro и в :guilabel:`Меню браузера` (1) выбрать пункт :guilabel:`Установить приложение` (2):

.. image:: _static/install_chrome.jpg
   :target: /_images/install_chrome.jpg
   :alt: установка в chrome

Установка в Safari
~~~~~~~~~~~~~~~~~~

Для установки мобильного приложения в Safari, необходимо перейти по ссылке https://app.avex.pro, нажать кнопку :guilabel:`Поделиться` (1) и в меню выбрать пункт :guilabel:`На экран Домой` (2):

.. image:: _static/install_safari.jpg
   :target: /_images/install_safari.jpg
   :alt: установка в safari
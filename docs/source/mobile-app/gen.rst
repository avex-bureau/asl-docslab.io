.. _мобильное-приложение:

Мобильное приложение
====================

.. toctree::

   install
   setup
   update-db
   report-form
   template
   send-report
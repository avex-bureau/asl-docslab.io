Выявление факторов опасности
============================

Вся поступающая в базу данных информация по безопасности полётов анализируется с точки зрения наличия угроз и факторов опасности :ref:`представителями по безопасности полётов <представители>` в подразделениях, каждый по своему направлению и окончательно :ref:`классифицируется <классификация-сообщений>` согласно разработанной таксономии.

Цель такого анализа — гарантировать, что сообщения в базе данных:

- содержат точную и проверенную информацию;
- содержат всю необходимую информацию;
- содержат ссылки на негативные факторы, предпосылки, события, инциденты, последствия и группы угроз, которые соответствуют текстовой части сообщения от персонала;

После получения сообщения, представитель по БП должен :ref:`принять его в работу <шапка-принятие-в-работу>` для дальнейшего анализа.

Основной задачей такого анализа является полная и правильная :ref:`классификация-сообщений`.

После уточнения всей необходимой информации и завершения работы с сообщением, сообщению необходимо :ref:`присвоить признак <шапка-подтверждение-сообщения>` :guilabel:`Подтверждено`, что подтверждает его полноту и точность.

Присвоение сообщению признака :guilabel:`Завершено` означает завершение блиц-расследования. 

Сроки принятия в работу и завершения расследования устанавливаются в каждой организации индивидуально, например:

- **Добровольные сообщения**:
    - принятие в работу - **72 часа**;
    - завершение - **14 дней**;
- **Сообщения по результатам АПИ**:
    - принятие в работу - **4 часа**;
    - завершение - **3 дня**.
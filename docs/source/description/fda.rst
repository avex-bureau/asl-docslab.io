.. _анализ-пи:

Анализ полётной информации
==========================

ASL позволяет вести учёт и обработку результатов анализа полётной информации.

Типовой сценарий работы
-----------------------

ASL может адаптироваться под различные сценарии работы с полётной информацией. Типовой сценарий описан далее.

#. в авиакомпании выполняется экспресс-анализ файлов полётной информации с помощью специального программного обеспечения;
#. результаты такого анализа вносятся в ASL;
#. при наличии событий экспресс-анализа (отклонений в лётной или технической части), они также :ref:`вносятся в ASL <апи-форма-отклонений>` после выполнения предварительной оценки на достоверность сотрудником участка расшифровки;
#. внесенная информация :ref:`автоматически через ASL поступает <настройка-фильтров>` потребителям (в первую очередь в лётный и технический комплексы);
#. по всем событиям экспресс-анализа :ref:`Представителями по БП <представители>` выполняется обработка и окончателный анализ поступившей информации, а также окончательная классификация сообщений;
#. на основании внесённой в ASL информации автоматически формируются журналы учёта ПИ. Организация может выбрать :ref:`упрощённый <апи-упрощённый-учёт>` или :ref:`полный <апи-полный-учёт>` учёт результатов АПИ.

.. note:: Вносить в ASL результаты экспресс-анализа и работать с ними могут только пользователи с соответствующими :ref:`правами <права>`.

.. _апи-упрощённый-учёт:

Упрощённый учёт результатов АПИ
-------------------------------

.. image:: _static/simple-table-view-fda.gif
    :target: /_images/simple-table-view-fda.gif

Упрощённый учёт включает в себя следующие элементы:

#. :ref:`упрощённую форму <апи-упрощённая-форма>` внесения результатов АПИ;
#. :ref:`статистику <апи-упрощённая-статистика>` результатов АПИ по каждому ВС;
#. :ref:`общую таблицу <апи-общая-таблица>` результатов АПИ по всему парку ВС.

.. _апи-упрощённая-форма:

Упрощённая форма
~~~~~~~~~~~~~~~~

| ``Путь:`` :ref:`основное-меню` --> :guilabel:`Анализ ПИ` --> :guilabel:`Внесение данных`.
| ``Права:`` :guilabel:`FDM - Вносить сообщения`

.. image:: _static/Screenshot_20240305_225000.png
    :target: /_images/Screenshot_20240305_225000.png
    
Упрощённая форма содержит следующие поля:

- :guilabel:`Дата обработки ПИ`;
- :guilabel:`Номер ВС`;
- :guilabel:`Дата первого рейса`;
- :guilabel:`Дата крайнего рейса`;
- :guilabel:`Расшифровано полётов`;
- :guilabel:`Отклонений`.

После выполнения экспресс-анализа серии полётов, седержащихся в файле полётной информации, сотрудник участка расшифровки выполняет предварителную оценку на достоверность полученных результатов и заполняет форму ASL для серии полётов.

Если по результатам предварительной оценки на достоверность отсутствуют подтверждённые события, то после нажатия кнопки :guilabel:`Продолжить` необходимо проверить введённую информацию и нажать кнопку :guilabel:`Сохранить`. Если были подтверждённые события - необходимо указать их количество в поле :guilabel:`Отклонений`, нажать кнопку :guilabel:`Продолжить` и :ref:`внести все события <апи-внесение-событий>`.

.. _апи-упрощённая-статистика:

Статистика по ВС
~~~~~~~~~~~~~~~~

.. include:: ../includes/fda-stat.inc

.. _апи-общая-таблица:

Общая таблица
~~~~~~~~~~~~~

| ``Путь:`` :ref:`основное-меню` --> :guilabel:`Анализ ПИ` --> :guilabel:`Журнал учёта ПИ` --> :guilabel:`Общая таблица`.
| ``Права:`` :guilabel:`FDM - Смотреть журналы`

Данные в таблице можно фильтровать по ключевым словам (2) или копировать / экспортировать в Excel (1).

.. image:: _static/Screenshot_20240309_032901.png
    :target: /_images/Screenshot_20240309_032901.png

.. hint:: Для горизонтальной прокрутки таблицы наведите на неё указатель мышки, зажмите на клавиатуре клавишу ``Shift`` и прокрутите колёсико. Также можно воспользоваться горизонтальной прокруткой на тачпаде.

Если в серии расшифрованных полётов имели место какие-либо отклонения, то они будут показаны в таблице в столбце :guilabel:`Отклонений`. В зависимости от :ref:`статуса <статус-сообщения>`, меняется подсветка номера сообщения (3).

Таблица формируется на основании заполнения :ref:`Упрощенной формы <апи-упрощённая-форма>`. Каждая запись таблицы может быть изменена (``Права:`` :guilabel:`FDM - Редактировать записи журналов`) или удалена (``Права:`` :guilabel:`FDM - Удалять записи журналов`). Для этого вконце каждой записи есть соответствующие кнопки.

.. important:: На данный момент история редактирования записей не сохраняется.




.. _апи-полный-учёт:

Полный учёт результатов АПИ
---------------------------

Полный учёт включает в себя следующие элементы:

#. :ref:`полную форму <апи-полная-форма>` внесения результатов АПИ;
#. :ref:`статистику <апи-полная-статистика>` результатов АПИ по каждому ВС;
#. :ref:`Журнал поступления <апи-журнал-поступления>` носителей полетной информации на обработку;
#. :ref:`Журнал выдачи <апи-журнал-выдачи>` результатов обработки полетной информации;
#. :ref:`Журнал отказов <апи-журнал-отказов>` и неисправностей авиатехники, выявленных при обработке полетной информации.




.. _апи-полная-форма:

Полная форма
~~~~~~~~~~~~

| ``Путь:`` :ref:`основное-меню` --> :guilabel:`Анализ ПИ` --> :guilabel:`Внесение данных`.
| ``Права:`` :guilabel:`FDM - Вносить сообщения`

.. image:: _static/Screenshot_20240305_225105.png
    :target: /_images/Screenshot_20240305_225105.png

Полная форма содержит следующие поля:

- :guilabel:`Поступление НПИ` - дата поступления файла на сервер;
- :guilabel:`Номер НПИ` - имя файла на сервере;
- :guilabel:`Сдал НПИ` - название организации или Ф.И.О. работника, выполнившего снятие ПИ;
- :guilabel:`Замечания к НПИ` - замечания к состоянию файла;
- :guilabel:`ВС` - воздушное судно;
- :guilabel:`Вид обработки` - экспресс-анализ / CVR / автоматизированная обработка или любое другое значение;
- :guilabel:`Обработка ПИ` - дата обработки;
- :guilabel:`Дата вылета`
- :guilabel:`Дата посадки`
- :guilabel:`Рейс`
- :guilabel:`Маршрут`
- :guilabel:`КВС`
- :guilabel:`Отклонений` - количество отклонений, выявленых на этапе предварительного анализа.

После выполнения экспресс-анализа серии полётов, седержащихся в файле полётной информации, сотрудник участка расшифровки выполняет предварителную оценку полученных результатов на достоверность и заполняет форму ASL для каждого рейса.

Заполнять заново все поля формы для каждого рейса нет необходимости, т.к. часть полей заполняется из предыдущего рейса. Для каждого последующего в серии полётов рейса нужно заполнить только поле :guilabel:`Рейс` (:guilabel:`Маршрут` подставится автоматически) и поля, требующие коректировки (например, :guilabel:`Дата вылета`, :guilabel:`Дата посадки`, :guilabel:`КВС`, :guilabel:`Отклонений`). Все изменённые поля для удобства подсвечиваются синим цветом.

.. image:: _static/Screenshot_20240305_232248.png
    :target: /_images/Screenshot_20240305_232248.png

Если по результатам предварительной оценки на достоверность отсутствуют подтверждённые события, то после нажатия кнопки :guilabel:`Продолжить` необходимо проверить введённую информацию и нажать кнопку :guilabel:`Сохранить`. Если были подтверждённые события - необходимо указать их количество в поле :guilabel:`Отклонений`, нажать кнопку :guilabel:`Продолжить` и :ref:`внести все события <апи-внесение-событий>`.




.. _апи-полная-статистика:

Статистика по ВС
~~~~~~~~~~~~~~~~

.. include:: ../includes/fda-stat.inc




.. _апи-журнал-поступления:

Журнал поступления
~~~~~~~~~~~~~~~~~~

| ``Путь:`` :ref:`основное-меню` --> :guilabel:`Анализ ПИ` --> :guilabel:`Журнал учёта ПИ` --> :guilabel:`Поступление`.
| ``Права:`` :guilabel:`FDM - Смотреть журналы`

Журнал учета поступления носителей полетной информации на обработку представлен в виде таблицы и содержит следующую информацию:

- :guilabel:`Поступление` - дата поступления файла на сервер;
- :guilabel:`ВС` - воздушное судно;
- :guilabel:`Посадка` - дата посадки;
- :guilabel:`Рейс`;
- :guilabel:`Носитель` - имя файла на сервере;
- :guilabel:`Сдал` - название организации или Ф.И.О. работника, выполнившего снятие ПИ;
- :guilabel:`Принял` - Ф.И.О. работника, который занёс результаты экспресс-анализа в ASL (заполняется автоматически при внесении);
- :guilabel:`Замечания` - замечания к состоянию файла.

.. image:: _static/Screenshot_20240309_040459.png
    :target: /_images/Screenshot_20240309_040459.png

.. hint:: Для горизонтальной прокрутки журнала наведите на него указатель мышки, зажмите на клавиатуре клавишу ``Shift`` и прокрутите колёсико. Также можно воспользоваться горизонтальной прокруткой на тачпаде.

Данные в журнале можно фильтровать по ключевым словам (2) или копировать / экспортировать в Excel (1).

Журнал формируется на основании заполнения :ref:`Полной формы <апи-полная-форма>`. Каждая запись журнала может быть изменена (``Права:`` :guilabel:`FDM - Редактировать записи журналов`) или удалена (``Права:`` :guilabel:`FDM - Удалять записи журналов`). Для этого вконце каждой записи есть соответствующие кнопки.

.. important:: На данный момент история редактирования записей не сохраняется.




.. _апи-журнал-выдачи:

Журнал выдачи
~~~~~~~~~~~~~

| ``Путь:`` :ref:`основное-меню` --> :guilabel:`Анализ ПИ` --> :guilabel:`Журнал учёта ПИ` --> :guilabel:`Выдача`.
| ``Права:`` :guilabel:`FDM - Смотреть журналы`

Журнал выдачи результатов обработки полетной информации представлен в виде таблицы и содержит следующую информацию:

- :guilabel:`Обработал` - Ф.И.О. работника, который занёс результаты экспресс-анализа в ASL (заполняется автоматически при внесении);
- :guilabel:`Дата` - дата обработки полётной информации;
- :guilabel:`Дата полёта`;
- :guilabel:`ВС`;
- :guilabel:`Рейс`;
- :guilabel:`Маршрут`;
- :guilabel:`КВС`;
- :guilabel:`Вид обработки` - экспресс-анализ / CVR / автоматизированная обработка или любое другое значение;
- :guilabel:`События` - номера :ref:`сообщений <сообщение>` с отклонениями;
- :guilabel:`Получил` - название подразделения, подставляется автоматически исходя из настроек организации;
- :guilabel:`Дата` - дата получения, заполняется автоматически датой внесения в ASL.

.. image:: _static/Screenshot_20240309_114448.png
    :target: /_images/Screenshot_20240309_114448.png

.. hint:: Для горизонтальной прокрутки журнала наведите на него указатель мышки, зажмите на клавиатуре клавишу ``Shift`` и прокрутите колёсико. Также можно воспользоваться горизонтальной прокруткой на тачпаде.

Данные в журнале можно фильтровать по ключевым словам (2) или копировать / экспортировать в Excel (1). Если в полёте имели место какие-либо отклонения, то они будут показаны в журнале в столбце :guilabel:`События`. В зависимости от :ref:`статуса <статус-сообщения>`, меняется подсветка номера сообщения (3).

Журнал формируется на основании заполнения :ref:`Полной формы <апи-полная-форма>`. Каждая запись журнала может быть изменена (``Права:`` :guilabel:`FDM - Редактировать записи журналов`) или удалена (``Права:`` :guilabel:`FDM - Удалять записи журналов`). Для этого вконце каждой записи есть соответствующие кнопки.

.. important:: На данный момент история редактирования записей не сохраняется.




.. _апи-журнал-отказов:

Журнал отказов
~~~~~~~~~~~~~~

| ``Путь:`` :ref:`основное-меню` --> :guilabel:`Анализ ПИ` --> :guilabel:`Журнал учёта ПИ` --> :guilabel:`Отказы`.
| ``Права:`` :guilabel:`FDM - Смотреть журналы`

Журнал учета отказов и неисправностей авиатехники представлен в виде таблицы и содержит следующую информацию:

- :guilabel:`ВС`
- :guilabel:`Обработано` - дата обработки полётной информации;
- :guilabel:`Дата полёта`
- :guilabel:`Рейс`
- :guilabel:`Описание` - описание выявленных неисправностей;
- :guilabel:`Выдано` - номер сообщения о выявленной неисправности и дата выдачи (заполняется автоматически при внесении)
- :guilabel:`Выдал` - Ф.И.О. работника, который занёс результаты экспресс-анализа в ASL (заполняется автоматически при внесении);
- :guilabel:`Принял` - название подразделения, подставляется автоматически исходя из настроек организации;

.. image:: _static/Screenshot_20240309_132715.png
    :target: /_images/Screenshot_20240309_132715.png

.. hint:: Для горизонтальной прокрутки журнала наведите на него указатель мышки, зажмите на клавиатуре клавишу ``Shift`` и прокрутите колёсико. Также можно воспользоваться горизонтальной прокруткой на тачпаде.

Данные в журнале можно фильтровать по ключевым словам (2) или копировать / экспортировать в Excel (1). Если в полёте имели место какие-либо отклонения, то они будут показаны в журнале в столбце :guilabel:`Выдано`. В зависимости от :ref:`статуса <статус-сообщения>`, меняется подсветка номера сообщения (3).

Все поля журнала заполняются на основании :ref:`Формы внесения отклонений <апи-форма-отклонений>` за исключением поля :guilabel:`Обработано` - данное поле заполняется на основании :ref:`Полной формы <апи-полная-форма>`.

.. note:: В процессе работы может возникнуть ситуация, когда запись в :ref:`журнале поступления <апи-журнал-поступления>` или :ref:`журнале выдачи <апи-журнал-выдачи>` удалена, а :ref:`сообщение <сообщение>` о выявленных неисправностях - нет. В таком случае в столбце :guilabel:`Обработано` будет гореть индикация :guilabel:`Нет записи` (4). В этом случае можно нажать на индикацию и внести недостающую информацию (``Права:`` :guilabel:`FDM - Вносить сообщения`).

Для редактирования поля :guilabel:`Обработано` необходимы ``Права:`` :guilabel:`FDM - Редактировать записи журналов`. Остальные поля журнала меняются путём редактирования :ref:`сообщений <сообщение>` о выявленных неисправностях (``Права:`` :guilabel:`FDM - Редактировать сообщения`). Для этого необходимо перейти на страницу сообщения, нажав на его номер в столбце :guilabel:`Выдано` (3).




.. _апи-форма-отклонений:

Внесение отклонений
-------------------

| ``Путь:`` :ref:`основное-меню` --> :guilabel:`Анализ ПИ` --> :guilabel:`Внесение данных` --> :guilabel:`Продолжить` --> :guilabel:`Добавить отклонение`.
| ``Права:`` :guilabel:`FDM - Вносить сообщения`

Если по результатам экспресс-анализа были выявлены отклонения (например, лётные или технические), то после заполнения :ref:`упрощённой <апи-упрощённая-форма>` или :ref:`полной <апи-полная-форма>` формы внесения результатов АПИ, ASL предложит внести эти отколонения. Для этого на экране появится форма внесения отклонений. Данная форма содержит следующие поля:

- :ref:`область`;
- :guilabel:`Аэропорт`;
- :guilabel:`Система ВС`;
- :guilabel:`Отклонения` (``обязательное``) - одно или несколько однотипных событий экспресс-анализа;
- :guilabel:`Шаблонный текст` - заранее заготовленный текст для описания отклонения;
- :guilabel:`Краткое описание` (``обязательное``);
- :guilabel:`Файлы` - любое количество файлов общим размером не более 300 МБ;
- :guilabel:`КВС`;
- :guilabel:`Второй пилот`.

.. image:: _static/Screenshot_20240309_214603.png
    :target: /_images/Screenshot_20240309_214603.png

Форму отклонений необходимо заполнить столько раз, сколько отклонений было указано на предыдущем этапе при внесении результатов экспресс-анализа.

.. hint:: :guilabel:`Шаблонный текст` позволяет существенно сократить время заполнения :guilabel:`Краткого описания` за счёт заранее заготовленного текста для отклонений. Шаблонный текст заполняется в :ref:`Административном интерфейсе <админка>` отдельно для каждого классификатора. Если шаблонный текст не указан - подставляется название самого классификатора.


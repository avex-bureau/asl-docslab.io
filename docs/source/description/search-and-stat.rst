.. _поиск-и-статистика:

Поиск и статистика
==================

.. TODO Поиск и статистика


ASL позволяет выполнять поиск и статистический анализ всех внесённых данных по безопасности полётов. Для этого необходимо нажать в :ref:`основном меню <основное-меню>` на пункт :guilabel:`Поиск и статистика`. После этого под ним раскроется блок подменю, который содержит следующие пункты:

.. toctree::

   stat-reports
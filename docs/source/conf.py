# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Документация ASL'
copyright = '2024, АВЭКС Бюро'
author = 'АВЭКС Бюро'
release = '0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
  'sphinxnotes.comboroles',
  ]

templates_path = ['_templates']
exclude_patterns = []

language = 'ru'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_logo = '_static/logo.svg'
html_static_path = ['_static']
html_copy_source = False
html_show_sourcelink = False

html_context = {
  'display_gitlab': True,
  'gitlab_host': 'gitlab.com',
  'gitlab_user': 'avex-bureau',
  'gitlab_repo': 'asl-docs.gitlab.io',
  'gitlab_version': 'master/docs/source/',
}


comboroles_roles = {
    'literal_ref': ['literal', 'ref'],
    'literal_doc': ['literal', 'doc'],
}
.. Документация ASL documentation master file, created by
   sphinx-quickstart on Sun Feb 25 01:12:41 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Документация ASL
================

Последнее изменение: |today|

.. toctree::
   :caption: Содержание
   :numbered:

   description/gen
   smm/gen
   mobile-app/gen
   implement/gen
   examples/gen
